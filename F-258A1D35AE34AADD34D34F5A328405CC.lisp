;;; This source file is part of the Meta-CVS program, 
;;; which is distributed under the GNU license.
;;; Copyright 2002 Kaz Kylheku

(eval-when (:compile-toplevel :load-toplevel :execute)
  (pushnew :clisp-unix-funcs *features*)
  #-cygwin (pushnew :linux *features*))

(defpackage :unix-funcs 
  (:use :common-lisp)
  (:shadowing-import-from :ffi 
    :def-call-out :def-c-struct :c-array-max :c-pointer :c-ptr :c-string :int
    :uint :ulong :ushort :long :boolean :character :c-array-ptr 
    :foreign-value :c-function :c-array :uint8 :uint16 :uint32 :uint64)
  (:shadow
    :open :close :signal)
  (:intern
    :def-c-call-out)
  (:export 
    :errno :strerror :eperm :enoent :esrch :eintr :eio
    :enxio :e2big :enoexec :ebadf :echild :eagain :enomem :eacces
    :efault :enotblk :ebusy :eexist :exdev :enodev :enotdir :eisdir
    :einval :enfile :emfile :enotty :etxtbsy :efbig :enospc :espipe
    :erofs :emlink :epipe :edom :erange :edeadlk :enametoolong :enolck
    :enosys :enotempty :eloop :ewouldblock :dirent :opendir :closedir
    :readdir :ino :name :open :close :chdir :fchdir :link :symlink
    :readlink :unlink :rmdir :stat :stat :lstat :fstat :chmod
    :mode :nlink :uid :pipe :fork :_exit :waitpid
    :gid :rdev :blksize :blocks :atime :mtime :ctime :s-ifmt :s-ifdir
    :s-ifchr :s-ifblk :s-ifreg :s-ififo :s-iflnk :s-ifsock :s-isdir
    :s-ischr :s-isblk :s-isreg :s-isfifo :s-islnk :s-issock :s-isuid
    :s-isgid :s-isvtx :s-iread :s-iwrite :s-iexec :s-irusr :s-iwusr
    :s-ixusr :s-irwxu :s-irgrp :s-iwgrp :s-ixgrp :s-irwxg :s-iroth
    :s-iwoth :s-ixoth :s-irwxo :accessperms :deffilemode :o-accmode
    :o-rdonly :o-wronly :o-rdwr :o-creat :o-excl :o-noctty :o-trunc
    :o-append :o-nonblock :o-sync :o-async :o-ndelay :o-fsync :getcwd
    :run-program default-sigchld ctermid))

(in-package :unix-funcs)
  
;;;
;;; A few macros to help condense the platform switching
;;;

(defmacro def-libc-call-out (name &rest args)
  `(def-call-out ,name 
     (:language :stdc) 
     #+cygwin (:library "cygwin1.dll") 
     #+linux (:library "libc.so.6")
     ,@args))

;;;
;;; <errno.h>
;;;

(progn
  (def-libc-call-out errno-location
    #+linux (:name "__errno_location")
    #+cygwin (:name "__errno")
    (:arguments)
    (:return-type (c-pointer int)))

  (defun get-errno ()
    (let ((loc (errno-location)))
      (foreign-value loc)))

  (defun set-errno (value)
    (let ((loc (errno-location)))
      (setf (foreign-value loc) value)))

  (defsetf get-errno set-errno)

  (define-symbol-macro errno (get-errno)))

(def-libc-call-out strerror
    (:arguments (errnum int))
    (:return-type c-string :none))

(defconstant eperm 1)
(defconstant enoent 2)
(defconstant esrch 3)
(defconstant eintr 4)
(defconstant eio 5)
(defconstant enxio 6)
(defconstant e2big 7)
(defconstant enoexec 8)
(defconstant ebadf 9)
(defconstant echild 10)
(defconstant eagain 11)
(defconstant enomem 12)
(defconstant eacces 13)
(defconstant efault 14)
(defconstant enotblk 15)
(defconstant ebusy 16)
(defconstant eexist 17)
(defconstant exdev 18)
(defconstant enodev 19)
(defconstant enotdir 20)
(defconstant eisdir 21)
(defconstant einval 22)
(defconstant enfile 23)
(defconstant emfile 24)
(defconstant enotty 25)
(defconstant etxtbsy 26)
(defconstant efbig 27)
(defconstant enospc 28)
(defconstant espipe 29)
(defconstant erofs 30)
(defconstant emlink 31)
(defconstant epipe 32)
(defconstant edom 33)
(defconstant erange 34)

;;;
;;; <dirent.h>
;;;

#+linux
(def-c-struct dirent
  ;; Actually this is struct dirent64
  (ino uint64)
  (off uint64)
  (reclen uint16)
  (type uint8)
  (name (c-array-max character 256)))

#+cygwin
(def-c-struct dirent
  (version uint32)
  (reserved (c-array uint32 2))
  (fd uint32)
  (ino uint32)
  (name (c-array-max character 256)))

(def-libc-call-out opendir 
  (:arguments (name c-string))
  (:return-type c-pointer))

(def-libc-call-out closedir 
  (:arguments (dirp c-pointer))
  (:return-type int))

(progn
  (def-libc-call-out readdir
    #+linux (:name "readdir64")
    #+cygwin (:name "readdir")
    (:arguments (dirp c-pointer))
    (:return-type (c-ptr dirent))))



;;;
;;; <unistd.h> -- open, close
;;;

(def-libc-call-out open 
  (:arguments (name c-string) 
	      (flags int) 
	      (mode uint))
  (:return-type int))

(def-libc-call-out close 
  (:arguments (fd int))
  (:return-type int))

;;;
;;; <unistd.h> -- chdir, fchdir
;;;

(def-libc-call-out chdir
  (:arguments (path c-string))
  (:return-type int))

(def-libc-call-out fchdir
  (:arguments (fd int))
  (:return-type int))

;;;
;;; <unistd.h> -- link, symlink, readlink, unlink, rmdir


(def-libc-call-out link 
  (:arguments (from c-string)
	      (to c-string))
  (:return-type int))

(def-libc-call-out symlink 
  (:arguments (from c-string)
	      (to c-string))
  (:return-type int))

(def-libc-call-out readlink-ll
  (:name "readlink")
  (:arguments (path c-string) 
              (buf (c-ptr (c-array-max character 4096)) :out :alloca) 
              (size ulong))
  (:return-type int))

(defun readlink (path)
  (multiple-value-bind (result link) 
                       (readlink-ll path 4096)
    (if (> result 0) link nil)))

(def-libc-call-out unlink
  (:arguments (path c-string))
  (:return-type int))

(def-libc-call-out rmdir 
  (:arguments (path c-string))
  (:return-type int))

;;;
;;; <unistd.h> -- stat, lstat, chmod
;;;

#+(and linux (not arch-x86_64))
(def-c-struct stat
  ;; actually, this is stat64
  (dev uint64)
  (__pad1 uint32)
  (__ino uint32)
  (mode uint32)
  (nlink uint32)
  (uid uint32)
  (gid uint32)
  (rdev uint64)
  (__pad2 uint32)
  (size uint64)
  (blksize uint32)
  (blocks uint64)
  (atime uint32)
  (atime-nsec uint32)
  (mtime uint32)
  (mtime-nsec uint32)
  (ctime uint32)
  (ctime-nsec uint32)
  (ino uint64))

#+(and linux arch-x86_64)
(def-c-struct stat
  ;; actually, this is stat64
  (dev uint64)
  (ino uint64)
  (nlink uint64)
  (mode uint32)
  (uid uint32)
  (gid uint32)
  (__pad0 uint32)
  (rdev uint64)
  (size uint64)
  (blksize uint64)
  (blocks uint64)
  (atime uint64)
  (atime-nsec uint64)
  (mtime uint64)
  (mtime-nsec uint64)
  (ctime uint64)
  (ctime-nsec uint64)
  (__unused (c-array uint64 3)))

#+cygwin
(def-c-struct stat
  (dev uint32)
  (ino uint64)
  (mode uint32)
  (nlink uint16)
  (uid uint32)
  (gid uint32)
  (rdev uint32)
  (size uint64)
  (atime uint32)
  (atime-nsec uint32)
  (mtime uint32)
  (mtime-nsec uint32)
  (ctime uint32)
  (ctime-nsec uint32)
  (blksize uint32)
  (blkcnt uint64)
  (spare4 (c-array uint32 2)))

#+linux
(progn
  #+arch-x86_64 (defconstant __stat-ver-linux 1)
  #-arch-x86_64 (defconstant __stat-ver-linux 3)

  (def-libc-call-out __xstat64
    (:arguments (version int)
                (name c-string) 
                (buf (c-ptr stat) :out))
    (:return-type int))

  (def-libc-call-out __lxstat64
    (:arguments (version int)
                (name c-string)
                (buf (c-ptr stat) :out))
    (:return-type int))

  (def-libc-call-out __fxstat64
    (:arguments (version int)
                (fd int) 
                (buf (c-ptr stat) :out))
    (:return-type int))

  (declaim (inline stat) (inline fstat) (inline lstat))

  (defun stat (name) (__xstat64 __stat-ver-linux name))
  (defun fstat (name) (__fxstat64 __stat-ver-linux name))
  (defun lstat (name) (__lxstat64 __stat-ver-linux name)))

#+cygwin
(progn
  (def-libc-call-out stat
    (:name "_stat64")
    (:arguments (name c-string) 
                (buf (c-ptr stat) :out))
    (:return-type int))

  (def-libc-call-out lstat
    (:name "_lstat64")
    (:arguments (name c-string)
                (buf (c-ptr stat) :out))
    (:return-type int))

  (def-libc-call-out fstat
    (:name "_fstat64")
    (:arguments (fd int) 
                (buf (c-ptr stat) :out))
    (:return-type int)))

(def-libc-call-out chmod
  (:arguments (name c-string)
	      (mode uint))
  (:return-type int))
	    
(defconstant s-ifmt   #o170000)
(defconstant s-ifdir  #o040000)
(defconstant s-ifchr  #o020000)
(defconstant s-ifblk  #o060000)
(defconstant s-ifreg  #o100000)
(defconstant s-ififo  #o010000)
(defconstant s-iflnk  #o120000)
(defconstant s-ifsock #o140000)

(defmacro s-isdir (m) `(= (logand ,m s-ifmt) s-ifdir))
(defmacro s-ischr (m) `(= (logand ,m s-ifmt) s-ifchr))
(defmacro s-isblk (m) `(= (logand ,m s-ifmt) s-ifblk))
(defmacro s-isreg (m) `(= (logand ,m s-ifmt) s-ifreg))
(defmacro s-isfifo (m) `(= (logand ,m s-ifmt) s-iffifo))
(defmacro s-islnk (m) `(= (logand ,m s-ifmt) s-iflnk))
(defmacro s-issock (m) `(= (logand ,m s-ifmt) s-ifsock))

(defconstant s-isuid  #o004000)
(defconstant s-isgid  #o002000)
(defconstant s-isvtx  #o001000)

(define-symbol-macro s-iread s-irusr)
(define-symbol-macro s-iwrite s-iwusr)
(define-symbol-macro s-iexec s-ixusr)

(defconstant s-irusr  #o000400)
(defconstant s-iwusr  #o000200)
(defconstant s-ixusr  #o000100)
(defconstant s-irwxu  (logior s-irusr s-iwusr s-ixusr))
(defconstant s-irgrp  #o000040)
(defconstant s-iwgrp  #o000020)
(defconstant s-ixgrp  #o000010)
(defconstant s-irwxg  (logior s-irgrp s-iwgrp s-ixgrp))
(defconstant s-iroth  #o000004)
(defconstant s-iwoth  #o000002)
(defconstant s-ixoth  #o000001)
(defconstant s-irwxo  (logior s-iroth s-iwoth s-ixoth))

(defconstant accessperms (logior s-irwxu s-irwxg s-irwxo))
(defconstant deffilemode (logior s-irusr s-iwusr s-irgrp s-iwgrp s-iroth s-iwoth))

;;;
;;; <unistd.h> -- pipe
;;;

(def-libc-call-out pipe
  (:arguments (filedes (c-ptr (c-array-max int 2)) :out))
  (:return-type int))

;;;
;;; <signal.h>
;;;


#+(or linux cygwin)
(progn
  (defconstant sig-err -1)
  (defconstant sig-dfl 0)
  (defconstant sig-ign 1))

#+linux
(progn
  (defconstant sighup 1)
  (defconstant sigint 2)
  (defconstant sigquit 3)
  (defconstant sigill 4)
  (defconstant sigtrap 5)
  (defconstant sigabrt 6)
  (defconstant sigiot 6)
  (defconstant sigbus 7)
  (defconstant sigfpe 8)
  (defconstant sigkill 9)
  (defconstant sigusr1 10)
  (defconstant sigsegv 11)
  (defconstant sigusr2 12)
  (defconstant sigpipe 13)
  (defconstant sigalrm 14)
  (defconstant sigterm 15)
  (defconstant sigstkflt 16)
  (defconstant sigchld 17)
  (defconstant sigcld sigchld)
  (defconstant sigcont 18)
  (defconstant sigstop 19)
  (defconstant sigtstp 20)
  (defconstant sigttin 21)
  (defconstant sigttou 22)
  (defconstant sigurg 23)
  (defconstant sigxcpu 24)
  (defconstant sigxfsz 25)
  (defconstant sigvtalrm 26)
  (defconstant sigprof 27)
  (defconstant sigwinch 28)
  (defconstant sigio 29)
  (defconstant sigpoll sigio)
  (defconstant sigpwr 30)
  (defconstant sigsys 31)
  (defconstant sigunused 31))

#+cygwin
(progn
  (defconstant sighup 1)
  (defconstant sigint 2)
  (defconstant sigquit 3)
  (defconstant sigill 4)
  (defconstant sigtrap 5)
  (defconstant sigabrt 6)
  (defconstant sigemt 7)
  (defconstant sigfpe 8)
  (defconstant sigkill 9)
  (defconstant sigbus 10)
  (defconstant sigsegv 11)
  (defconstant sigsys 12)
  (defconstant sigpipe 13)
  (defconstant sigalrm 14)
  (defconstant sigterm 15)
  (defconstant sigurg 16)
  (defconstant sigstop 17)
  (defconstant sigtstp 18)
  (defconstant sigcont 19)
  (defconstant sigchld 20)
  (defconstant sigcld 20)
  (defconstant sigttin 21)
  (defconstant sigttou 22)
  (defconstant sigio 23)
  (defconstant sigpoll sigio)
  (defconstant sigxcpu 24)
  (defconstant sigxfsz 25)
  (defconstant sigvtalrm 26)
  (defconstant sigprof 27)
  (defconstant sigwinch 28)
  (defconstant siglost 29)
  (defconstant sigusr1 30)
  (defconstant sigusr2 31))

(def-libc-call-out signal-ll
 (:name "signal")
 (:arguments (num int)
             (handler (c-function (:language :stdc)
                                  (:arguments (num int))
                                  (:return-type))))
 (:return-type ulong))

(def-libc-call-out signal-hack-ll
 (:name "signal")
 (:arguments (num int) (handler ulong))
 (:return-type ulong))

(declaim (inline signal))
(defun signal (num func)
  (if (functionp func)
    (signal-ll num func)
    (signal-hack-ll num func)))

;;;
;;; <unistd.h> -- getcwd
;;;

(progn
  (def-libc-call-out getcwd-ll
    (:name "getcwd")
    ;; this is char on purpose, since we take the returned value,
    ;; and ignore the :out parameter.  It would be a waste of cycles
    ;; to put it through a charset encoder.
    (:arguments (buf (c-ptr (c-array-max char 4096)) :out :alloca) 
                (size ulong))
    (:return-type c-string))

  (defun getcwd ()
    (values (getcwd-ll 4096))))

;;;
;;; <unistd.h> -- fork, wait*, exec*
;;;

(defun default-sigchld ()
  (signal sigchld sig-dfl))

(def-libc-call-out fork
  (:arguments)
  (:return-type int))

(def-libc-call-out waitpid
  (:arguments (pid int) (status (c-ptr int) :out) (options int))
  (:return-type int))

(def-libc-call-out execvp
  (:arguments (file c-string) (argv (c-array-ptr c-string)))
  (:return-type int))

(def-libc-call-out _exit
  (:arguments (status int))
  (:return-type))

(defmacro wexitstatus (status) `(ash (logand ,status #xff00) -8))

;;; if wifsignaled is true, gives the terminating signal
(defmacro wtermsig (status) `(logand ,status #x7f))

;;; if wifstopped is true, gives signal that stopped child
(defmacro wstopsig (status) `(wexitstatus ,status))

(defmacro wifexited (status) `(zerop (wtermsig ,status)))

(defmacro wifsignaled (status) `(< 0 (logand ,status #x7f) 0x7f))

(defmacro wifsignaled (status) `(= (logand ,status #x7f) 0x7f))

(defconstant wnohang 1)
(defconstant wuntraced 2)
(defconstant wcontinued 3)

(defun spawn (name argument-vector)
  (default-sigchld)
  (let ((child (fork)))
    (cond
      ((< child 0) nil)
      ((zerop child)
       (execvp name argument-vector)
       (_exit 1))
      (t (loop
           (multiple-value-bind (result status) 
                                (waitpid child 0)
             (if (or (>= result 0) (/= eintr errno))
               (when (wifexited status)
                 (return (wexitstatus status)))
               (return result))))))))

(defun run-program (name &key arguments)
  (push name arguments)
  (spawn name (coerce arguments 'vector)))

;;;
;;; Terminal related functions
;;;

(defconstant l-ctermid 9)

(def-libc-call-out ctermid-ll 
 (:name "ctermid")
 (:arguments (buf (c-ptr (c-array-max char 10)) :out)) 
 (:return-type c-string))

(defun ctermid ()
  (values (ctermid-ll)))

;;;
;;; <fcntl.h>
;;;

(defconstant o-accmode  #o00003)
(defconstant o-rdonly   #o00000)
(defconstant o-wronly   #o00001)
(defconstant o-rdwr     #o00002)
(defconstant o-creat    #o00100)
(defconstant o-excl     #o00200)
(defconstant o-noctty   #o00400)
(defconstant o-trunc    #o01000)
(defconstant o-append   #o02000)
(defconstant o-nonblock #o04000)
(defconstant o-sync     #o10000)
(defconstant o-async    #o20000)
(defconstant o-ndelay o-nonblock)
(defconstant o-fsync o-sync)
