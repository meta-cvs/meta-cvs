#!/bin/sh

LATEXDOC=meta-cvs

rm $LATEXDOC.aux $LATEXDOC.log $LATEXDOC.dvi $LATEXDOC.toc \
  $LATEXDOC.idx $LATEXDOC.ind $LATEXDOC.ilg \
  $LATEXDOC.idx.old $LATEXDOC.toc.old
