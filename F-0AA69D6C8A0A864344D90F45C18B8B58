Things to do in short time frame:

- import should know things about some suffixes, like what is ``obviously''
  binary, and what should probably be ignored.
- When an add wants to clobber a local file with a newly added file, provide a
  restart which does the reverse clobber instead. Also interactive handling
  to do this on a file by file basis might be nice. [2002.06.21]
- optimize split-words similarly to split-fields.
- Automatically synchronize sandbox symlink changes back to map [2002.08.25]
- Do something with symlinks in remap command [2002.08.26]
- Recognize when running over a case-insensitive filesystem, and adjust
  the path-equal function accordingly. Also make sure the right comparison
  function is used everywhere. The posix.lisp module should also export a
  function that is suitable as an argument to :test for make-hash-table.
  [2002.09.25]
- Substitute more informative messages for low-level file access messages
  coming out of CLISP [2002.10.05]
- Repository branching command [2003.03.24].
- Local and repository ``ls'' [2003.03.24].
- Support for rtag and rlog commands [2003.03.24].
- use -f option in some cases to suppress the reading of ~/.cvsrc.

Longer term things:

- stop representing paths as strings, at least internally! This is sheer
  stupidity.
- confusing behavior: mcvs mv * subdir causes all files from the 
  current directory to be removed, which causes the current directory
  itself to be removed. The directory is then re-created with subdir,
  and the files moved there. However, the shell is left in an orphaned
  directory.
- cvs unedit has funny reverting behavior with respect to the time stamp.
  it doesn't behave like update -C; rather it reverts the contents and
  the timestamp too, so Meta-CVS thinks the reverted F-file is older.
- deferred cvs add: do not invoke cvs add for new files until commit
  time. This will work around a nasty cvs add bug. 
  [2002.04.04] [Coded and tested, put in deferred-adds-branch]
- when files are added on a managed branch and committed, the special tags 
  should be set in these files accordingly. [2002.04.03]
- optimize move command, it performs badly when the number of
  arguments is large. [2002.02.02]
- make *argument-limit* controllable from command line.
- dirwalk-fi function should put out canonicalized path names. [2002.01.27]
- develop repository-side migration tool to convert CVS module to Meta-CVS 
  form. [2002.01.27]
