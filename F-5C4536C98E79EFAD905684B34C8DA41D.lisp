;;; This source file is part of the Meta-CVS program, 
;;; which is distributed under the GNU license.
;;; Copyright 2002 Kaz Kylheku

(in-package :meta-cvs)

(defconstant *argument-limit* (* 64 1024))

(defun execute-program-xargs (fixed-args &optional extra-args fixed-trail-args)
  (let* ((fixed-size (reduce #'(lambda (x y)
				 (+ x (length y) 1))
			     (append fixed-args fixed-trail-args)
			     :initial-value 0))
	 (size fixed-size))
    (if extra-args
      (let ((chopped-arg ())
	    (combined-status t))
	(dolist (arg extra-args)
	  (push arg chopped-arg)
	  (when (> (incf size (1+ (length arg))) *argument-limit*)
	    (setf combined-status 
		  (and combined-status
		       (execute-program (append fixed-args 
						(nreverse chopped-arg)
						fixed-trail-args))))
	    (setf chopped-arg nil)
	    (setf size fixed-size)))
	(when chopped-arg
	  (execute-program (append fixed-args (nreverse chopped-arg)
				   fixed-trail-args)))
	combined-status)
      (execute-program (append fixed-args fixed-trail-args)))))
