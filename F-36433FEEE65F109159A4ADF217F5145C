;;; This source file is part of the Meta-CVS program, 
;;; which is distributed under the GNU license.
;;; Copyright 2002 Kaz Kylheku

(in-package :meta-cvs)

(defun make-filt-hash (mapping)
  (let ((h (make-hash-table :test #'equal)))
    (dolist (entry mapping h)
      (multiple-value-bind (suffix nosuffix)
			   (suffix (mapping-entry-id entry))
	(declare (ignore suffix))
	(setf (gethash nosuffix h) entry)))))

(defun filt-select-map (filt-options &key remote-module)
  (find-bind (:test #'string= :key #'first :take #'second)
	     ((revision "r") (date "D") (extra-r "r") (extra-d "D")) 
	     filt-options
    (cond
      ((or extra-r extra-d)
	 (error "only one date or revision may be specified."))
      ((or revision date remote-module)
	 (unless remote-module
           (chdir *admin-dir*))
	 (with-input-from-program (stream `("cvs" "-Q" 
					    ,(if remote-module "co" "up") "-p"
					    ,@(format-opt filt-options)
					    ,(if remote-module 
					       (format nil "~a/~a"
						       remote-module
						       *map-file*)
					       *map-file*)))
	   (handler-case
	     (mapping-read stream)
	     (error ()
	       (error "unable to retrieve specified revision of map file.")))))
      (t (mapping-read *map-local-path*)))))

(defun filt-loop (filehash)
  (loop
    (let ((line (read-line *standard-input* nil)))	
      (when (null line) 
	(return (values)))
      (loop
	(let ((f-start (search "F-" line :test #'char=))
	      (embedded-in-path (search "/F-" line :test #'char=))
	      (cvs-backup-notation (search ".#F-" line :test #'char=)))
	  (flet ((is-hex-digit (x) 
		   (or (digit-char-p x)
		       (find x "ABCDEF"))))
	    (cond
	      ((and embedded-in-path (< embedded-in-path f-start))
		(write-string (subseq line 0 (+ embedded-in-path 3)))
		(setf line (subseq line (+ embedded-in-path 3))))
	      ((and cvs-backup-notation (< cvs-backup-notation f-start))
		(write-string (subseq line 0 cvs-backup-notation))
		(write-string *admin-dir*)
		(write-string "/.#F-")
		(setf line (subseq line (+ cvs-backup-notation 4))))
	      (f-start
		(write-string (subseq line 0 f-start))
		(setf line (subseq line (+ f-start 2)))
		(when (< (length line) 32)
		  (write-string "F-")
		  (write-line line)
		  (return))
		(cond 
		  ((notevery #'is-hex-digit (subseq line 0 32))
		     (write-string "F-")
		     (setf line (subseq line 2)))
		  (t (let* ((f-digits (subseq line 0 32))
			    (entry (gethash (format nil "F-~a" f-digits)
					    filehash))
			    (suffix (and entry 
					 (suffix (mapping-entry-id entry)))))
		       (setf line (subseq line 32))
		       (cond
			 ((null entry)
			    (write-string "F-")
			    (write-string f-digits))
			 ((and suffix 
			       (or (< (length line) (1+ (length suffix)))
				   (not (path-equal (subseq line 1 
							       (1+ (length suffix)))
						    suffix))))
			    (write-string "F-")
			    (write-string f-digits))
			 (t (write-string (mapping-entry-path entry))
			    (when suffix
			      (setf line 
				    (subseq line 
					       (1+ (length suffix)))))))))))
	      (t (write-line line)
		 (return)))))))))

(defun filt (filt-options)
  (in-sandbox-root-dir
    (filt-loop (make-filt-hash (filt-select-map filt-options)))))

(defun remote-filt (filt-options module)
  (filt-loop (make-filt-hash (filt-select-map filt-options 
						   :remote-module module))))

(defun filt-wrapper (cvs-options cvs-command-options mcvs-args)
  (declare (ignore cvs-options))
  (when mcvs-args
    (error "no arguments permitted."))
  (filt cvs-command-options))

(defun remote-filt-wrapper (cvs-options cvs-command-options mcvs-args)
  (declare (ignore cvs-options))
  (unless (= (length mcvs-args) 1)
    (error "module name required."))
  (remote-filt cvs-command-options (first mcvs-args)))
