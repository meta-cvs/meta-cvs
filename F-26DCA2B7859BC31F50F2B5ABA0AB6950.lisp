;;; This source file is part of the Meta-CVS program, 
;;; which is distributed under the GNU license.
;;; Copyright 2002 Kaz Kylheku

(in-package :meta-cvs)

(defvar *edit-program* nil)

(defun arglist-to-command-string (arglist)
"Convert list of strings, assumed to be an argument vector, into
a single command string that can be submitted to a POSIX command 
interpreter. This requires escaping of all shell meta-characters."
  (let ((command (make-array '(1024)
			     :element-type 'character
			     :adjustable t
			     :fill-pointer 0)))
    (dolist (arg arglist command)
      (dotimes (i (length arg))
	(let ((ch (char arg i)))
	  (when (find ch #(#\' #\" #\* #\[ #\] #\? 
			   #\$ #\{ #\} #\" #\space #\tab
			   #\( #\) #\< #\> #\| #\; #\&))
	    (vector-push-extend #\\ command))
	  (vector-push-extend ch command)))
	(vector-push-extend #\space command))))

(defun invoke-editor-on (name)
  (let ((editor (or *edit-program* 
		    (env-lookup "CVSEDITOR")
		    (env-lookup "VISUAL")
		    (env-lookup "EDITOR" "vi"))))
    (execute-program `(,editor ,name))))
