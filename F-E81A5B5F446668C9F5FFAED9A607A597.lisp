;;; This source file is part of the Meta-CVS program, 
;;; which is distributed under the GNU license.
;;; Copyright 2002 Kaz Kylheku

(defpackage :meta-cvs
  (:nicknames :mcvs)
  (:use :common-lisp)
  (:shadow :merge)
  (:export main main-debug))
