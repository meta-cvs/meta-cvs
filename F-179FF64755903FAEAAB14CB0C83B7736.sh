file_list="$file_list"' unix.o'
mod_list="$mod_list"' unix'
make clisp-module CC="${CC}" CFLAGS="${CFLAGS}" INCLUDES="$absolute_linkkitdir"
NEW_FILES="$file_list wrap.o"
NEW_LIBS="$file_list wrap.o"
NEW_MODULES="$mod_list"
TO_LOAD='unix'

# hack to get our own main()

FILES="$(echo $FILES | sed -e 's/lisp.a //')"
objcopy --redefine-sym main=clisp_main "$absolute_sourcedir"/lisp.a "$absolute_destinationdir"/lisp.a
