;;; This source file is part of the Meta-CVS program, 
;;; which is distributed under the GNU license.
;;; Copyright 2002 Kaz Kylheku

(in-package :meta-cvs)

(defun purge (global-options)
  (in-sandbox-root-dir
    (let* ((filemap (mapping-read *map-path* :sanity-check t))
	   (to-be-removed (mapping-removed-files filemap)))
      (when to-be-removed
	(chdir *admin-dir*)
	 (chatter-debug "Invoking CVS.~%")
	 (unless (execute-program-xargs `("cvs" ,@(format-opt global-options)
					  "rm" "-f")
					(mapcar #'basename to-be-removed))
	   (error "CVS rm failed.")))))
  (values))

(defun purge-wrapper (global-options command-options args)
  (declare (ignore command-options))
  (when args
    (error "no arguments permitted."))
  (purge global-options))
